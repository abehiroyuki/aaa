package com.example.beans;

import java.util.HashMap;

import android.graphics.Bitmap;

public class CommentBeans {
	
	private CommentWebDAO mWebDAO;
	
	String[] fieldName = {};
	
	public CommentBeans(){
		mWebDAO = new CommentWebDAO();
	}
	
	public CommentDTO getComment() throws Exception{
		return mWebDAO.select();
	}
	
	public void getImage(CommentDTO commentDTO) throws Exception{
		HashMap<String, Bitmap> imgMap = commentDTO.getMap();
		for(String key : imgMap.keySet()){
			if( imgMap.get(key) == null ){
				if(!getImageFromSQLite()){
					commentDTO.setSelectedImageId(key);
//					getImageFromRemoteDB(commentDTO);
					mWebDAO.getImage(commentDTO);
//					return;
				}
			}
		}
	}
	
	private boolean getImageFromSQLite(){
		return false;
	}

}
