package com.example.connent;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.content.ContentValues;

public class RestUtil {
	
	public static final RestTask1 obtainGetTaskNoThread(String url, boolean isBinary)
			throws MalformedURLException, IOException {
		HttpURLConnection connection = (HttpURLConnection)(new URL(url)).openConnection();
		connection.setReadTimeout(15000);
		connection.setConnectTimeout(15000);
		connection.setDoInput(true);
		Get get = new Get(connection);
		if(isBinary) get.setIsBinary();
		RestTask1 task1 = new RestTask1(get);
		return task1;
	}
    
    public static final RestTask2 obtainGetTask(String url, boolean isBinary)
            									throws MalformedURLException, IOException {
    	HttpURLConnection connection = (HttpURLConnection)(new URL(url)).openConnection();
        connection.setReadTimeout(15000);
        connection.setConnectTimeout(15000);
        connection.setDoInput(true);
        Get get = new Get(connection);
        if(isBinary) get.setIsBinary();
        RestTask2 task2 = new RestTask2(get);
    	return task2;
    }
    
    public static final RestTask1 obtainFormPostTaskNoThread(String url, ContentValues values, boolean isBinary)
    		throws MalformedURLException, IOException {
    	HttpURLConnection connection = (HttpURLConnection)(new URL(url)).openConnection();
    	connection.setReadTimeout(15000);
    	connection.setConnectTimeout(15000);
    	connection.setDoOutput(true);
    	Post post = new Post(connection, values);
    	RestTask1 task1 = new RestTask1(post);
    	return task1;
    }
    
    public static final RestTask2 obtainFormPostTask(String url, ContentValues values, boolean isBinary)
																			throws MalformedURLException, IOException {

    	HttpURLConnection connection = (HttpURLConnection)(new URL(url)).openConnection();
    	connection.setReadTimeout(15000);
    	connection.setConnectTimeout(15000);
    	connection.setDoOutput(true);
    	Post post = new Post(connection, values);
    	RestTask2 task2 = new RestTask2(post);
    	return task2;
    }
    
    public static final RestTask1 obtainMultipartPostTaskNoThread(String url,
    		ContentValues values, File file, String fileName)
            throws MalformedURLException, IOException {
        
    	HttpURLConnection connection = (HttpURLConnection)(new URL(url)).openConnection();
    	connection.setDoOutput(true);
        connection.setReadTimeout(15000);
        connection.setConnectTimeout(15000);
        connection.setRequestMethod("POST");
        
        PostImage pi = new PostImage(connection, values, file, fileName);
        RestTask1 task1 = new RestTask1(pi);
        return task1;
    }
    
    public static final RestTask2 obtainMultipartPostTask(String url,
    		ContentValues values, File file, String fileName)
            throws MalformedURLException, IOException {
        
    	HttpURLConnection connection = (HttpURLConnection)(new URL(url)).openConnection();
    	connection.setDoOutput(true);
        connection.setReadTimeout(15000);
        connection.setConnectTimeout(15000);
        connection.setRequestMethod("POST");
        
        RestTask2 task2 = null;
//        task2.setUploadFile(file, fileName);
//        task2.setSendMethod("POST");
        return task2;
    }

}
