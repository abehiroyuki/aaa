package com.example.beans;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import com.example.connent.RestTask1;
import com.example.connent.RestUtil;

import android.content.ContentValues;

public class MemberWebDAO implements DAO {
	
	static final String INSERT_URL = "http://3-dot-habe37books.appspot.com/insertMember";
	static final String IMAGE_UPLOAD_URL = "http://3-dot-habe37books.appspot.com/upload";

	@Override
	public DTO select() throws Exception {
		return null;
	}

	@Override
	public Object insert(DTO dto) throws Exception {
		MemberDTO memberDTO = (MemberDTO)dto;
		ContentValues cv = memberDTO.getContentValues();
		RestTask1 rest1 = RestUtil.obtainFormPostTaskNoThread(INSERT_URL, cv, false);
		return rest1.connect();
	}
	
	
	public Object uploadImage(MemberDTO memberDTO) throws Exception{
		File uploadImgFile = memberDTO.getUpImgFile();
		String fileName = memberDTO.getFilePath();
		RestTask1 rest1 = RestUtil.obtainMultipartPostTaskNoThread(IMAGE_UPLOAD_URL, null, uploadImgFile , fileName);
		return rest1.connect();
	}

}
