package com.example.twitter2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beans.CommentDTO;

public class MyListAdapter1 extends BaseAdapter {
	
	private Context mContext = null;
	private CommentDTO mData = null;
	private int mResource = 0;
	
	public MyListAdapter1(Context context, CommentDTO data, int resouce){
		mContext = context;
		mData  = data;
		mResource = resouce;
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public CommentDTO getItem(int position) {
		return (CommentDTO) mData.get(position);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = mInflater.inflate(mResource, null);
		CommentDTO comment = getItem(position);
		
		TextView txtDate = (TextView)convertView.findViewById(R.id.text_date);
		TextView txtName = (TextView)convertView.findViewById(R.id.text_name);
		TextView txtComment = (TextView)convertView.findViewById(R.id.text_comment);
		ImageView sumIView = (ImageView)convertView.findViewById(R.id.sum_img);
		
		txtDate.setText(comment.getDate());
		txtName.setText(comment.getUserId());
		txtComment.setText(comment.getComment());
		sumIView.setImageBitmap(comment.getBitmap());
		
		return convertView;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	

}
