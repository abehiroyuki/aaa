package com.example.twitter2.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Rect;
import android.util.Log;

public class ImageIO {
	
	
	public static void saveImage(File path, Bitmap bmp){
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(path);
			bmp.compress(CompressFormat.JPEG, 100, out);
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static Bitmap[] trimImage(String path, Bitmap bitmap){
		int height = bitmap.getHeight();
		int width = bitmap.getWidth();
	    BitmapRegionDecoder regionDecoder;
	    Bitmap[] bitmapArr = new Bitmap[2];
	    try {
			regionDecoder = BitmapRegionDecoder.newInstance(path, false);
			Rect rect1 = new Rect(0, 0, width/2, height);
			Rect rect2 = new Rect(width/2, 0, width, height);
		    bitmapArr[0] = regionDecoder.decodeRegion(rect1, null);
		    bitmapArr[1] = regionDecoder.decodeRegion(rect2, null);
//		    saveImage(new File(MainActivity.dataPath + "/" + fileName + "x.jpg") , bitmap1);
//		    saveImage(new File(MainActivity.dataPath + "/" + fileName + "y.jpg") , bitmap2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return bitmapArr;
	}
	
	public static void outputImgFile(String filePath, Bitmap bitmap, byte[] jpegData){
		File file = new File(filePath);
		try{
			if (file.createNewFile()) {	
				FileOutputStream outputStream = new FileOutputStream(file);	
				if (bitmap != null) {			
					bitmap.compress(CompressFormat.JPEG, 75, outputStream);	
				} else {				
					outputStream.write(jpegData);
				}
			}
		}catch(Exception e){
			Log.v("outoutImgFile", e.getMessage());
		}
	}
	
	public static Bitmap getImgFile(String filePath){
		File srcFile = new File(filePath);
		Bitmap bitmap = null;
		try{
			FileInputStream fis = new FileInputStream(srcFile);
			bitmap = BitmapFactory.decodeStream(fis);
			fis.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return bitmap;
	}
	
}
