package com.example.connent;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Map;

import android.content.ContentValues;
import android.util.Log;

public class Post extends SendMethod {
	
	protected String mFormBody = null;
    
	public Post(HttpURLConnection connection, ContentValues values){
		mConnection = connection;
		setFormBody(values);
		mBoundary = generateBoundary();
	}

	@Override
	public void setRequest() throws IOException {
		if(mFormBody != null){
			mConnection.setRequestProperty("Content-Type",
								"application/x-www-form-urlencoded; charset=" + mCharset);
		}
	}
	
	@Override
	public void output() throws IOException{
		BufferedWriter writer = null;
		Log.v("abe", "up開始");
		if(mFormBody != null){
			try{
				OutputStream out = mConnection.getOutputStream();
				writer = new BufferedWriter( new OutputStreamWriter(out, Charset.forName(mCharset)),  8192);
    			writer.write(mFormBody);
    		}finally{
    			if(writer != null){
    				writer.close();
    			}
    		}
		}
	}
	
	protected void setFormBody(ContentValues values){
    	if(values == null){
    		mFormBody = null;
    		return;
    	}
    	int i = 0;
    	String str = "";
    	for(Map.Entry<String, Object> entry : values.valueSet()) {
    		String key = entry.getKey();
    	    String value = entry.getValue().toString();
    		str += URLEncoder.encode(key);
    		str += "=";
    		str += URLEncoder.encode(value);
    		i++;
    		if(i < values.size()){
    			str += "&";
    		}
    	}
    	Log.v("abe", str);
    	mFormBody = str;
    }

}
