package com.example.facade;

import android.os.AsyncTask;

import com.example.beans.CommentDTO;
import com.example.beans.MemberBeans;
import com.example.beans.MemberDTO;

public class SetMemberFacade extends AsyncTask<MemberDTO, Object, Object>{

	Callback mCallback;
	
	public interface Callback{
		void setAdapter(CommentDTO commentDTO);
	}
	
	public SetMemberFacade(Callback callback){
		mCallback = callback;
	}

	@Override
	protected Object doInBackground(MemberDTO... memberDTOArr) {
		Object result = null;
		MemberDTO memberDTO = memberDTOArr[0];
		MemberBeans mb = new MemberBeans();
		try {
			mb.setMember(memberDTO);
			mb.setImage(memberDTO);
		} catch (Exception e) {
			e.printStackTrace();
			result = e;
		}
		return result;
	}
	
	@Override
    protected void onPostExecute(Object obj) {
		if(obj instanceof CommentDTO){
			mCallback.setAdapter((CommentDTO)obj);
		}
    }

}
