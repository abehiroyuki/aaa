package com.example.twitter2.util;

import java.io.File;
import java.util.ArrayList;

import android.os.Environment;

//�t�@�C�������֘A��Util
public class FileProcess {
	
	
	public static int searchFile(File[] files, String target){
		int i = 0;
		while(i < files.length && files[i].isFile() &&
						files[i].getName().indexOf(target) == -1){
			i++;
		}
		if(i < files.length){
			return i;
		}
		return -1;
	}
	
	
	//�t�@�C���ꗗ�̎擾
	public static File[] getFiles(String path){
		File file = new File(path);
		File[] files = null;
		if( new File(path).isDirectory()){
			files = file.listFiles();
		}
		return files;
	}
	
	public static File[] getFiles(){
		String subPath = Environment.getExternalStorageDirectory().getPath();
		File file = new File(subPath);
		File[] files = null;
		if( new File(subPath).isDirectory()){
			files = file.listFiles();
		}
		ArrayList<File> fileList = new ArrayList<File>();
		for(int i=0; i<files.length; i++){
			if(files[i].isFile() && 
					(files[i].getName().endsWith("jpg") || files[i].getName().endsWith("png") || 
							files[i].getName().endsWith("bmp"))){
				fileList.add( files[i] );
			}
		}
		File[] fileArr = fileList.toArray(new File[0]);
		return fileArr;
	}
	
	public static String[] getNamesFromFiles(File[] files){
		 String[] names = new String[files.length];
		 for(int i=0; i<files.length; i++){
			 names[i] = files[i].getAbsolutePath();
		 }
		 return names;
	}
	
	
	
	
}
