package com.example.twitter2;

import com.example.beans.CommentDTO;
import com.example.facade.GetCommentFacade;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.GetChars;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

public class CommentListFragment extends BaseFragment 
                                 implements GetCommentFacade.Callback{
	
	ListView mListView;
	Button mUpBtn, mLoginBtn;
	CommentListFragment glf;
	CommentDTO mCommentDTO = null;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        glf = this;
    }
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.comment_list_fragment, null);
        return view;
    }
	
	@Override
	public void onStart() {
		super.onStart();
		mListView = (ListView) getActivity().findViewById(R.id.listview);
		mUpBtn = (Button)getActivity().findViewById(R.id.up_btn);
		mUpBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				GetCommentFacade gcb = new GetCommentFacade(glf);
				gcb.execute();
			}
		});
//		mLoginBtn = (Button)getActivity().findViewById(R.id.login_btn);
//		mLoginBtn.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
////				FragmentManager manager = getActivity().getSupportFragmentManager();
////				Fragment mrf = new MemberRegistFragment();
////				FragmentTransaction transaction = manager.beginTransaction();
////				transaction.replace( R.id.fragment, mrf, "test" );
////				//バックスタックに変更前の状態を追加する
////				transaction.addToBackStack( "first" );
////				transaction.commit();
//			}
//		});
		if(mCommentDTO != null) setAdapter(mCommentDTO);
	}

	@Override
	public void notif() {

	}
	
	@Override
	public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
		// TODO Auto-generated method stub
		 //＊＊＊FragmentTransactionにTRANSIT_FRAGMENT_OPENを指定しておくと、遷移時にはTRANSIT_FRAGMENT_OPEN、Back時にはTRANSIT_FRAGMENT_CLOSEが渡される＊＊＊
//        if (transit == FragmentTransaction.TRANSIT_FRAGMENT_OPEN) {
//            if (enter) {
//                return AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in);
//            } else {
//            	return AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in);
//            }
//        } else if (transit == FragmentTransaction.TRANSIT_FRAGMENT_CLOSE) {
//            if (enter) {
//            	return AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in);
//            } else {
//            	return AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in);
//            }
//        }
//        return super.onCreateAnimation(transit, enter, nextAnim);
        return AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in);
	}

	@Override
	public void setAdapter(CommentDTO cDTO) {
		mCommentDTO = cDTO;
		mListView.setAdapter(new MyListAdapter1(getActivity(), cDTO, R.layout.list_item1));
	}

}
