package com.example.twitter2;

import com.example.facade.GetCommentFacade;
import com.example.twitter2.MyDialogFragment.Callback;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.Button;

public class MemberRegistFragment extends BaseFragment implements 
                                                       MyDialogFragment.Callback{
	
	private Button mRegistBtn;
	private MemberRegistFragment mFragment;
	private Callback mCallback;
	
	int containerId;
	int containerWidth;
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Object callback = getParentFragment();
        if (callback == null) {
            callback = getActivity();
            if (callback == null || !(callback instanceof Callback)) {
                throw new IllegalStateException();
            }
        }
        mCallback = (Callback) callback;
    }
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		containerId = container.getId();
        //＊＊＊コンテナの幅を保存しておく＊＊＊
        containerWidth = container.getWidth();
        return inflater.inflate(R.layout.member_regist_fragment, null);
    }
	
	@Override
	public void onStart() {
		super.onStart();
		mFragment = this;
		mRegistBtn = (Button) getActivity().findViewById(R.id.member_regist_btn);
		mRegistBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new MyDialogFragment.Builder(mFragment, new MemberRegistDialog())
				                    .title("aaaa")
				                    .negative("CANCEL")
				                    .positive("OK")
				                    .resouceId(R.layout.member_regist_dialog)
				                    .show();
			}			
		});
	}
	
	@Override
	public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
		// TODO Auto-generated method stub
		 //＊＊＊FragmentTransactionにTRANSIT_FRAGMENT_OPENを指定しておくと、遷移時にはTRANSIT_FRAGMENT_OPEN、Back時にはTRANSIT_FRAGMENT_CLOSEが渡される＊＊＊
        if (transit == FragmentTransaction.TRANSIT_FRAGMENT_OPEN) {
            if (enter) {
                return AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in);
            } else {
            	return AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in);
            }
        } else if (transit == FragmentTransaction.TRANSIT_FRAGMENT_CLOSE) {
            if (enter) {
            	return AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in);
            } else {
            	return AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in);
            }
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
	}

	@Override
	public void notif() {
		
	}

	@Override
	public void onMyDialogSucceeded(int requestCode, int resultCode,
			Bundle params) {
		
		
	}

	@Override
	public void onMyDialogCancelled(int requestCode, Bundle params) {
		// TODO Auto-generated method stub
		
	}


}
