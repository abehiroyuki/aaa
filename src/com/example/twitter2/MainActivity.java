package com.example.twitter2;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beans.CommentDTO;
import com.example.facade.GetCommentFacade;

public class MainActivity extends BaseActivity implements GetCommentFacade.Callback{
	
	String name, password, imgFileName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		ActionBar actionBar = getActionBar();
//		actionBar.setDisplayShowTitleEnabled(false);
//		actionBar.setDisplayShowHomeEnabled(false);
//		
//		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
//		createTab(actionBar, new MainTabListener<CommentListFragment>(this, CommentListFragment.class, "a"), "タブ1");
//		createTab(actionBar, new MainTabListener<MemberRegistFragment>(this, MemberRegistFragment.class, "b"), "タブ2");
		
		Fragment mrf = new CommentListFragment();
		getSupportFragmentManager().
		         beginTransaction().
		         add(R.id.fragment, mrf ).
		         setTransition(0x10000).
		         commit();
	}
	
	@Override
	public void onClick(DialogInterface dialog, int buttonId) {
		super.onClick(dialog, buttonId);
	}

	@Override
	public void setAdapter(CommentDTO cDTO) {
		Toast.makeText(this, "成功", Toast.LENGTH_SHORT).show();
		lView.setAdapter(new MyListAdapter1(this, cDTO, R.layout.list_item1));
	}
	
	
	public static class MainTabListener<T extends Fragment> implements ActionBar.TabListener {

		private Fragment mFragment;
		private final MainActivity mActivity;
		private final Class<T> mClass;
		private android.support.v4.app.FragmentTransaction fft;
		private String mTag;

		public MainTabListener(MainActivity activity, Class<T> cls, String tag) {
			this.mActivity = activity;
			this.mClass = cls;
			this.mTag = tag;
		}

		@Override
		public void onTabSelected(Tab tab,
				FragmentTransaction ft) {
			mFragment = mActivity.getSupportFragmentManager().findFragmentByTag(mTag);
			fft = mActivity.getSupportFragmentManager().beginTransaction();

			if (mFragment == null) {

				// If not, instantiate and add it to the activity
				mFragment = Fragment.instantiate(mActivity, mClass.getName());
				fft.add(R.id.fragment, mFragment, mTag);
				//fft.add(android.R.id.content, mFragment, mTag);
				fft.commit();
				mActivity.getSupportFragmentManager().executePendingTransactions();
			} else {

				// If it exists, simply attach it in order to show it
				fft.attach(mFragment);
				fft.commit();
				mActivity.getSupportFragmentManager().executePendingTransactions();
			}
		}

		@Override
		public void onTabUnselected(Tab tab,
				FragmentTransaction ft) {
			mFragment = mActivity.getSupportFragmentManager().findFragmentByTag(mTag);

			if (mFragment != null && !mFragment.isDetached()) {
				fft = mActivity.getSupportFragmentManager().beginTransaction();
				fft.detach(mFragment);
				fft.commit();
				mActivity.getSupportFragmentManager().executePendingTransactions();
			}
		}

		@Override
		public void onTabReselected(Tab tab,
				android.app.FragmentTransaction ft) {
			// TODO Auto-generated method stub

		}
	}
	 
	 private void createTab(ActionBar actionBar, MainTabListener<?> listener, String title) {
	        ActionBar.Tab tab = actionBar.newTab();
	        tab.setTabListener(listener);
	        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        View v = inflator.inflate(R.layout.custom_tab, null);
	        tab.setCustomView(v);
	        TextView tv = (TextView) tab.getCustomView().findViewById(R.id.text_tab);
	        tv.setText(title);
	        tv.setWidth(1900);
	        actionBar.addTab(tab);
//	        ActionBar ab = getActionBar();
//	        Tab tab2 = (Tab) ab.getTabAt(0)
	        
	        Log.i("abe", "abe");
	    }
	

}
