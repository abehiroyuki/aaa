package com.example.connent;


public class RestTask1 {
	
	private SendMethod mSm;
	
	public RestTask1(SendMethod sm) {
        mSm = sm;
    }
	
	public Object connect() throws Exception{
		Object obj = mSm.connect();
		if (obj instanceof Exception) {
            throw new Exception();
        }else{
        	return obj;
        }
	}
	
	public void setRequestProperty(String field, String newValue){
		mSm.setEntity(field, newValue);
	}
}
