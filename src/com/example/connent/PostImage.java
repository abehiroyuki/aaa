package com.example.connent;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;

import android.content.ContentValues;
import android.util.Log;

public class PostImage extends Post{
	
    private File mUploadFile = null;
    private String mFileName = "";
	
	public PostImage(HttpURLConnection connection, ContentValues values, File uploadFile, String fileName){
		super(connection, values);
		mUploadFile = uploadFile;
		mFileName = fileName;
	}
	
	@Override
	public void setRequest() throws IOException{
//		setRequest();
		if(mUploadFile != null){
			mConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + mBoundary);
		}
	}
	
	@Override
	public void output() throws IOException{
		super.output();
		OutputStream out = mConnection.getOutputStream();
//		BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(out, Charset.forName(mCharset)),  8192);
		DataOutputStream dos = new DataOutputStream(out);
		Log.v("abe", "img_up開始");
		if(mUploadFile != null){
			String str = "--" + mBoundary + "\r\n";
			str += "Content-Disposition: form-data; name=\"MyFile\"; filename=\"";
			//str += uploadFile.getName();
			str += mFileName;
			str += "\"\r\n";
			str += "Content-Type: text/plain\r\n\r\n";
			dos.writeBytes(str);
			dos.flush();
			Log.v("abe", str);
			InputStream input = new FileInputStream(mUploadFile);
			byte[] buffer = new byte[1024];
			for(int length = 0; (length = input.read(buffer)) > 0;){
				out.write(buffer, 0, length);
			}
			out.flush();
			input.close();
			dos.writeBytes("\r\n");
			dos.writeBytes("--" + mBoundary + "--");
			dos.writeBytes("\r\n");
			dos.flush();
			dos.close();
		}
	}

}
