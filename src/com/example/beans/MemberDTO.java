package com.example.beans;

import java.io.File;

import android.content.ContentValues;

public class MemberDTO extends DTO{
	
	private String mUserId = "";
	private String mPassword = "";
	private String mName = "";
	private String mImageId = "";
	private File mUpImgFile;
	@Override
	public void setVal(String[] strArr) {
	}
	
	public ContentValues getContentValues(){
		ContentValues cv = new ContentValues();
		cv.put("userId", getUserId());
		cv.put("password", getPassword());
		cv.put("name", getName());
		cv.put("imageId", getFilePath());
		return cv;
	}
	
	public String getUserId() { return mUserId; }
	public void setUserId(String mUserId) { this.mUserId = mUserId;}
	public String getPassword() { return mPassword; }
	public void setPassword(String mPassword) { this.mPassword = mPassword; }
	public String getName() { return mName;}
	public void setName(String mName) { this.mName = mName;}
	public String getFilePath() { return mImageId; }
	public void setFilePath(String mFilePath) {this.mImageId = mFilePath; }
	public void setUpImgFile(File file){this.mUpImgFile = file;}
	public File getUpImgFile(){return mUpImgFile;}

}
