package com.example.facade;

import android.os.AsyncTask;

import com.example.beans.CommentBeans;
import com.example.beans.CommentDTO;

public class GetCommentFacade extends AsyncTask<CommentDTO, Object, Object>{

	Callback mCallback;
	
	public interface Callback{
		void setAdapter(CommentDTO commentDTO);
	}
	
	public GetCommentFacade(Callback callback){
		mCallback = callback;
	}

	@Override
	protected Object doInBackground(CommentDTO... params) {
		CommentBeans cb = new CommentBeans();
		Object result = null;
		try {
			CommentDTO cDTO = cb.getComment();
			cb.getImage(cDTO);
			result = (Object)cDTO;
		} catch (Exception e) {
			e.printStackTrace();
			result = e;
		}
		return result;
	}
	
	@Override
    protected void onPostExecute(Object obj) {
		if(obj instanceof CommentDTO){
			mCallback.setAdapter((CommentDTO)obj);
		}
    }

}
