package com.example.beans;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.connent.RestTask1;
import com.example.connent.RestUtil;

public class CommentWebDAO implements DAO{
	
	static final String SELECT_URL = "http://3-dot-habe37books.appspot.com/commentSelect";
	static final String GET_URL = "http://3-dot-habe37books.appspot.com/upload";
	
	@Override
	public CommentDTO select() throws Exception {
		RestTask1 task1 = RestUtil.obtainFormPostTaskNoThread(SELECT_URL, null, false);
		Object result = task1.connect();
		CommentDTO commentDTO = new CommentDTO();
		commentDTO.setDTOList((String)result);
		return commentDTO;
	}
	
	public void getImage(CommentDTO commentDTO) throws Exception{
		String url = GET_URL + "?file=" + commentDTO.getSelectedImageId();
		RestTask1 task1 = RestUtil.obtainGetTaskNoThread(url, true);
		Object result = task1.connect();
		byte[] byteArr = (byte[])result;
		Log.v("abe成功", byteArr.toString());
		BitmapFactory.Options bfOptions = new BitmapFactory.Options();
		bfOptions.inPurgeable = true;
		Bitmap resultBitmap = BitmapFactory.decodeByteArray(byteArr, 0, byteArr.length);
		commentDTO.setImageMap(resultBitmap);
	}

	@Override
	public Object insert(DTO dto) throws Exception {
		CommentDTO commentDTO = (CommentDTO)dto;
		return null;
	}

}
