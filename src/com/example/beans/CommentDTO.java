
package com.example.beans;

import java.net.URLDecoder;
import java.util.HashMap;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class CommentDTO extends DTO implements Parcelable{
	
	private String mDate;
	private String mUserId;
	private String mComment;
	private String mImageId;
	
	private String mSelectedImageId;
	private HashMap<String, Bitmap> mImgMap = new HashMap<String, Bitmap>();
	
	public CommentDTO(){
//		mDTOList.clear();
//		mImgMap.clear();
	}
	
	@Override
	public void setVal(String[] strArr) {
		CommentDTO messDTO = new CommentDTO();
		messDTO.setDate(strArr[0]);
		messDTO.setUserId(URLDecoder.decode(strArr[1]));
		messDTO.setComment(URLDecoder.decode(strArr[2]));
		messDTO.setImageId(strArr[3]);
		mImgMap.put(strArr[3], null);
		mDTOList.add(messDTO);
	}
	
	public HashMap<String, Bitmap> getMap(){
		return mImgMap;
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		
	}
	
	public String getUserId() { return mUserId; }
	public void setUserId(String mUserId) { this.mUserId = mUserId; }


	public String getComment() {
		return mComment;
	}


	public void setComment(String mComment) {
		this.mComment = mComment;
	}


	public String getDate() {
		return mDate;
	}


	public void setDate(String mDate) {
		this.mDate = mDate;
	}
	
	public String getImageId(){
		return mImageId;
	}
	
	public void setImageId(String imageId){
		mImageId = imageId;
	}
	
	public String getSelectedImageId(){
		return mSelectedImageId;
	}
	
	public void setSelectedImageId(String selectedImageId){
		mSelectedImageId = selectedImageId;
	}
	
	public void setImageMap(Bitmap b){
		mImgMap.put(mSelectedImageId, b);
	}
	
	public Bitmap getBitmap(){
		return mImgMap.get(mImageId);
	}

	

}
