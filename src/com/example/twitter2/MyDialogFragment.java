package com.example.twitter2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MyDialogFragment extends DialogFragment {
	
    private Callback mCallback;
    
	public interface Callback {
        void onMyDialogSucceeded(int requestCode, int resultCode, Bundle params);
        void onMyDialogCancelled(int requestCode, Bundle params);
    }

    public static class Builder {

        final Activity mActivity;
        final Fragment mParentFragment;
        final MyDialogFragment mDialog;

        String mTitle;
        String mMessage;
        String[] mItems;
        String mPositiveLabel;
        String mNegativeLabel;
        int mRequestCode = -1;
        int mResId;
        Bundle mParams;
        String mTag = "default";
        boolean mCancelable = true;

        public <A extends Activity & Callback> Builder(final A activity, final MyDialogFragment dialog) {
            mActivity = activity;
            mParentFragment = null;
            mDialog = dialog;
        }

        public <F extends Fragment & Callback> Builder(final F parentFragment, MyDialogFragment dialog) {
            mParentFragment = parentFragment;
            mActivity = null;
            mDialog = dialog;
        }

        public Builder title(final String title) {
            mTitle = title;
            return this;
        }

        public Builder title(final int title) {
            return title(getContext().getString(title));
        }

        public Builder message(final String message) {
            mMessage = message;
            return this;
        }

        public Builder message(final int message) {
            return message(getContext().getString(message));
        }

        public Builder items(final String... items) {
            mItems = items;
            return this;
        }

        public Builder positive(final String positiveLabel) {
            mPositiveLabel = positiveLabel;
            return this;
        }

        public Builder positive(final int positiveLabel) {
            return positive(getContext().getString(positiveLabel));
        }

        public Builder negative(final String negativeLabel) {
            mNegativeLabel = negativeLabel;
            return this;
        }

        public Builder negative(final int negativeLabel) {
            return negative(getContext().getString(negativeLabel));
        }

        public Builder requestCode(final int requestCode) {
            mRequestCode = requestCode;
            return this;
        }

        public Builder tag(final String tag) {
            mTag = tag;
            return this;
        }

        public Builder params(final Bundle params) {
            mParams = new Bundle(params);
            return this;
        }

        public Builder cancelable(final boolean cancelable) {
            mCancelable = cancelable;
            return this;
        }
        
        public Builder resouceId(final int resId){
        	mResId = resId;
        	return this;
        }

        /**
         * DialogFragment を Builder に設定した情報を元に show する.
         */
        public void show() {
            final Bundle args = new Bundle();
            args.putString("title", mTitle);
            args.putString("message", mMessage);
            args.putStringArray("items", mItems);
            args.putString("positive_label", mPositiveLabel);
            args.putString("negative_label", mNegativeLabel);
            args.putBoolean("cancelable", mCancelable);
            args.putInt("resid", mResId);
            if (mParams != null) {
                args.putBundle("params", mParams);
            }

//            final MyDialogFragment f = new MyDialogFragment();
            if (mParentFragment != null) {
            	mDialog.setTargetFragment(mParentFragment, mRequestCode);
            } else {
                args.putInt("request_code", mRequestCode);
            }
            mDialog.setArguments(args);
            if (mParentFragment != null) {
            	mDialog.show(mParentFragment.getChildFragmentManager(), mTag);
            } else {
            	mDialog.show(((FragmentActivity) mActivity).getSupportFragmentManager(), mTag);
            }
        }

        private Context getContext() {
            return (mActivity == null ? mParentFragment.getActivity() : mActivity).getApplicationContext();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Object callback = getParentFragment();
        if (callback == null) {
            callback = getActivity();
            if (callback == null || !(callback instanceof Callback)) {
                throw new IllegalStateException();
            }
        }
        mCallback = (Callback) callback;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final DialogInterface.OnClickListener listener = new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(which == Dialog.BUTTON_POSITIVE){
					positiveButtonClick();
				}
				dismiss();
	            mCallback.onMyDialogSucceeded(getRequestCode(), which, getArguments().getBundle("params"));
			}
		};
        final String title = getArguments().getString("title");
        final String message = getArguments().getString("message");
        final String[] items = getArguments().getStringArray("items");
        final String positiveLabel = getArguments().getString("positive_label");
        final String negativeLabel = getArguments().getString("negative_label");
        final int resId = getArguments().getInt("resid");
        setCancelable(getArguments().getBoolean("cancelable"));
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        if (!TextUtils.isEmpty(message)) {
            builder.setMessage(message);
        }
        if (items != null && items.length > 0) {
            builder.setItems(items, listener);
        }
        if (!TextUtils.isEmpty(positiveLabel)) {
            builder.setPositiveButton(positiveLabel, listener);
        }
        if (!TextUtils.isEmpty(negativeLabel)) {
            builder.setNegativeButton(negativeLabel, listener);
        }
        if(resId != 0){
        	LayoutInflater li = LayoutInflater.from(getActivity());
        	View view = li.inflate(resId, null);
        	createView(view);
        	builder.setView(view);
        }
        return builder.create();
    }
    
    public void createView(View view){};
    public void positiveButtonClick(){};

    @Override
    public void onCancel(DialogInterface dialog) {
        mCallback.onMyDialogCancelled(getRequestCode(), getArguments().getBundle("params"));
    }
    
    
   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
		   Bundle savedInstanceState) {
	   return super.onCreateView(inflater, container, savedInstanceState);
   }
   
   @Override
   public void onStart() {
	   super.onStart();
	   
   }

    /**
     * リクエストコードを取得する. Activity と ParentFragment 双方に対応するため.
     *
     * @return requestCode
     */
    private int getRequestCode() {
        return getArguments().containsKey("request_code") ? getArguments().getInt("request_code") : getTargetRequestCode();
    }
}
