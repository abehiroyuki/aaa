package com.example.beans;

import java.util.ArrayList;

public abstract class DTO {
	
	protected ArrayList<DTO> mDTOList = new ArrayList<DTO>();
	
	public int size(){
		return mDTOList.size();
	}
	
	public DTO get(int position){
		return mDTOList.get(position);
	}
	
	public void setDTOList(String result){
		String[] strArr = result.split("&");
		if(strArr.length > 0){
			for(int i=0; i<strArr.length; i++){
				String[] strArr2 = strArr[i].split("=");
				if(strArr2.length > 2) setVal(strArr2);
			}
		}
	}
	
	public abstract void setVal(String[] strArr);

}
