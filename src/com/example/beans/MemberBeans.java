package com.example.beans;

public class MemberBeans {
	
	MemberWebDAO mMemberWebDAO;
	
	public MemberBeans(){
		mMemberWebDAO = new MemberWebDAO();
	}
	
	public boolean isValidMember(){
		return false;
	}
	
	public void setMember(MemberDTO memberDTO) throws Exception{
		mMemberWebDAO.insert(memberDTO);
	}
	
	public void setImage(MemberDTO memberDTO) throws Exception{
		if(memberDTO.getUpImgFile() == null){
			return;
		}
		mMemberWebDAO.uploadImage(memberDTO);
	}
	
	

}
