package com.example.beans;

import android.app.Activity;

import com.example.twitter2.MyDialogFragment.Callback;

public interface DAO {
	
	public DTO select() throws Exception;
	public Object  insert(DTO dto) throws Exception;

}
