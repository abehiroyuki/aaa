package com.example.twitter2.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class CCal {
	public int year, month, day, hour, minute, second;
	
	private int maxday;
	
	Calendar cal;
	SimpleDateFormat df;

	public CCal(){
		this.cal = Calendar.getInstance();
	}
	
	public void getCurrentTime(){
		this.cal = Calendar.getInstance();
		this.year = cal.get(Calendar.YEAR);
		this.month = cal.get(Calendar.MONTH);
		this.day = cal.get(Calendar.DATE);
		this.hour = cal.get(Calendar.HOUR);
		this.minute = cal.get(Calendar.MINUTE);
		this.second = cal.get(Calendar.SECOND);
	}
	
	
	
	public String getCurrentDateStr(){
		Date date = cal.getTime();												//���݂̓���擾
		this.df = new SimpleDateFormat("yyyyMMdd");
		return df.format(date);
	}
	
	public String getCurrentSecondStr(){
		Date date = cal.getTime();
		return new SimpleDateFormat("yyyyMMddHHmmss").format(date);
	}
	
	public String getCurrentHourStr(){
		Date date = cal.getTime();												
		this.df = new SimpleDateFormat("HHmmss");
		return df.format(date);
	}
	
	public int getMaxday(){
		return maxday;
	}
	
	public void progressDate(int date){
		cal.add(Calendar.DATE, date);
	}
	
	public void getDate(){
		Date date = cal.getTime();
		System.out.print(df.format(date));
	}

	public String timeKeika(int hour1, int hour2, int minute1, int minute2){
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		
		Calendar a = Calendar.getInstance();
		Calendar b = Calendar.getInstance();
		Calendar c = Calendar.getInstance();
		
		a.set(Calendar.HOUR_OF_DAY, hour1);
		a.set(Calendar.MINUTE, minute1);
		b.set(Calendar.HOUR_OF_DAY, hour2);
		b.set(Calendar.MINUTE, minute2);
		
		long sa = b.getTimeInMillis() - a.getTimeInMillis()
											- c.getTimeZone().getRawOffset();
		c.setTimeInMillis(sa);
		
		String[] s = sdf.format(c.getTime()).split(":");
		int hour3 = Integer.parseInt(s[0]);
		int minute3 = Integer.parseInt(s[1]);

		return String.format("%d��%d��",hour3, minute3);
		}
	}
