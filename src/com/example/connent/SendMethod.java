package com.example.connent;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.Random;

import android.util.Log;

public abstract class SendMethod {
	
	protected String mBoundary;
	protected String mFormBody = null;
	protected HttpURLConnection mConnection = null;
	protected String mCharset = "UTF-8";
	private boolean mIsBinary;
	
	public Object connect(){
		Object obj = null;
		try {
			setRequest();
			mConnection.connect();
			output();
			if(mIsBinary){
				obj = inputByteArr();
			}else{
				obj = inputString();
			}
		} catch (IOException e) {
			obj = e;
			e.printStackTrace();
		}
		return obj;
	}
	
	public abstract void setRequest() throws IOException;
	public abstract void output() throws IOException;
	
	private String inputString() throws IOException{
		InputStream stream = mConnection.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
		String responseData = null;
		StringBuilder sb = new StringBuilder();
		while((responseData = reader.readLine()) != null){
			sb.append(responseData);
		}
		stream.close();
		return sb.toString();
	}
	
	private byte[] inputByteArr() throws IOException{
		InputStream stream = mConnection.getInputStream();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		byte[] byteChunk = new byte[1024];
		int bytesRead = 0;
		while( (bytesRead = stream.read(byteChunk)) != -1) {
			byteArrayOutputStream.write(byteChunk, 0, bytesRead);
			Log.v("abe", String.valueOf(bytesRead));
		}
		byte[] byteArray = byteArrayOutputStream.toByteArray();
		return byteArray;
	}
	
	public void setIsBinary(){
		mIsBinary = true;
	}

	protected String generateBoundary(){
	    String chars = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_";
	    Random rand = new Random();
	    String boundary = "";
	    for(int i = 0; i < 40; i++){
	        int r = rand.nextInt(chars.length());
	        boundary += chars.substring(r, r + 1);
	    }
	    return boundary;
	}
	
	public void setEntity(String field, String newValue){
		mConnection.setRequestProperty(field, newValue);
	}

}
