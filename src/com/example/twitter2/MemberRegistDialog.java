package com.example.twitter2;

import java.io.File;
import java.util.Date;

import com.example.beans.CommentDTO;
import com.example.beans.MemberDTO;
import com.example.facade.SetMemberFacade;
import com.example.twitter2.util.FileProcess;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MemberRegistDialog extends MyDialogFragment implements SetMemberFacade.Callback,
MyDialogFragment.Callback{
	
	private Button mImageSelectBtn;
	private EditText mIdEdit, mNameEdit, mPassEdit;
	private TextView mImgPathTxt;
	private MemberRegistDialog mMrg;
	private File mUpImgFile;
	private File[] mFiles;
	
	@Override
	public void createView(View view) {
		mMrg = this;
		mImageSelectBtn = (Button) view.findViewById(R.id.img_select_btn);
		mIdEdit = (EditText)view.findViewById(R.id.id_edit);
		mPassEdit= (EditText)view.findViewById(R.id.pass_edit);
		mNameEdit = (EditText)view.findViewById(R.id.name_edit);
		mImgPathTxt = (TextView)view.findViewById(R.id.img_path_text);
		mImageSelectBtn.setOnClickListener(new android.view.View.OnClickListener() {
   			@Override
   			public void onClick(View view) {
   				mFiles = FileProcess.getFiles();
   				String[] items = FileProcess.getNamesFromFiles(mFiles);
   				new MyDialogFragment.Builder(mMrg, new MyDialogFragment())
                                    .title("画像選択")
                                    .negative("CANCEL")
                                    .items(items)
                                    .show();
   			}
   		});
	}
	
	
	@Override
	public void positiveButtonClick() {
		super.positiveButtonClick();
		MemberDTO memberDTO = new MemberDTO();
		memberDTO.setUserId(mIdEdit.getText().toString());
		memberDTO.setPassword(mPassEdit.getText().toString());
		memberDTO.setName(mNameEdit.getText().toString());
		Long date = System.currentTimeMillis(); 
		memberDTO.setFilePath(date.toString());
		memberDTO.setUpImgFile(mUpImgFile);
		SetMemberFacade smf = new SetMemberFacade(mMrg);
		smf.execute(memberDTO);
	}


	@Override
	public void setAdapter(CommentDTO commentDTO) {
		Toast.makeText(getActivity(), "成功", Toast.LENGTH_SHORT).show();
	}


	@Override
	public void onMyDialogSucceeded(int requestCode, int resultCode,
			Bundle params) {
		if(mFiles != null){
			//Toast.makeText(getActivity(), mFiles[resultCode].getAbsolutePath(), Toast.LENGTH_SHORT).show();
			mUpImgFile = mFiles[resultCode];
			mImgPathTxt.setText(mUpImgFile.getAbsolutePath());
		}
	}


	@Override
	public void onMyDialogCancelled(int requestCode, Bundle params) {
		// TODO Auto-generated method stub
		
	}

}
