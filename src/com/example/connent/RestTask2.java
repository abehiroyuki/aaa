
package com.example.connent;

import java.io.IOException;
import java.lang.ref.WeakReference;

import android.os.AsyncTask;

public class RestTask2 extends AsyncTask<Void, Integer, Object> {
    
    boolean isBinary, isArr;
    
    private WeakReference<ResponseCallback> mResponseCallback;
    private SendMethod mSm;
    
    public interface ResponseCallback {
        public void onRequestSuccess(Object result);
        public void onRequestError(Exception error);
    }
    
    public RestTask2(SendMethod sm) {
        mSm = sm;
    }
    
    public void isBinary(boolean isBinary){
    	this.isBinary = isBinary;
    }
    
    public void setResponseCallback(ResponseCallback callback) {
        mResponseCallback = new WeakReference<ResponseCallback>(callback);
    }
    
    @Override
    protected Object doInBackground(Void... params) {
    	Object obj = mSm.connect();
		return obj;
    }
    
    @Override
    protected void onPostExecute(Object result) {
        if (mResponseCallback != null && mResponseCallback.get() != null) {
            if (result instanceof String || result instanceof byte[]) {			//�ʐM������
                mResponseCallback.get().onRequestSuccess(result);
            } else if (result instanceof Exception) {							//�ʐM���s��
                mResponseCallback.get().onRequestError((Exception) result);
            } else {
                mResponseCallback.get().onRequestError(new IOException("Unknown Error Contacting Host"));
            }
        }
    }
}
