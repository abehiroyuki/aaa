package com.example.twitter2.util;

public class TextEdit {
	
	public static String ymdEdit(String ymdStr){
		String year = "";
		String month = "";
		String date = "";
		try{
			year = ymdStr.substring(0, 4);
			month = ymdStr.substring(4, 6);
			date= ymdStr.substring(6, 8);
		}catch(Exception e){
			
		}
		return String.format("%s/%s/%s", year, month, date);
	}
	
	public static String hmsEdit(String hmsStr){
		String hour = "";
		String minute = "";
		String second = "";
		try{
			hour = hmsStr.substring(0, 2);
			minute = hmsStr.substring(2, 4);
			second= hmsStr.substring(4, 6);
		}catch(Exception e){
			
		}
		return String.format("%s:%s:%s", hour, minute, second);
	}
	
	public static String ymdhmsEdit(String ymdhmsStr){
		String year = "";
		String month = "";
		String day = "";
		String hour = "";
		String minute = "";
		String second = "";
		try{
			year = ymdhmsStr.substring(0, 4);
			month = ymdhmsStr.substring(4, 6);
			day = ymdhmsStr.substring(6, 8);
			hour = ymdhmsStr.substring(8, 10);
			minute= ymdhmsStr.substring(10, 12);
			second = ymdhmsStr.substring(12, 14);
		}catch(Exception e){
			e.printStackTrace();
		}
		return String.format("%s/%s/%s %s:%s:%s", year,month, day, hour, minute, second);
	}
	


}
