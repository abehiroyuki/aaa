package com.example.connent;

import java.io.IOException;
import java.net.HttpURLConnection;

public class Get extends SendMethod {
	
	public Get(HttpURLConnection connection){
		mConnection = connection;
	}

	@Override
	public void setRequest() throws IOException {
		mConnection.setRequestProperty("Accept-Charset",mCharset);
	}

	@Override
	public void output() throws IOException {

	}

}
