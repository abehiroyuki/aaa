
package com.example.twitter2;



import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.twitter2.util.CCal;
import com.example.twitter2.util.FileProcess;

public class BaseActivity extends FragmentActivity implements DialogInterface.OnClickListener{
	
	public static String subPath, appPath;
	
	//ダイアログ値保存用
	public static SharedPreferences.Editor editor;
	public static SharedPreferences sp;
	public static final String PRE_FILE_NAME = "PreFile";
	
	ListView lView;
	MyListAdapter1 myAdapter1;
	Button upBtn, loginBtn;
	Button testBtn;
	EditText comEtxt;
	
	AlertDialog dialog1, dialog2;
	AlertDialog listDialog, customDialog1, customDialog2;
	AlertDialog.Builder dg;
	
	String saveListItem;
	
	//カスタムダイアログ
	TextView imgPathTxt;
	EditText nameETxt, passETxt;
	
	ProgressDialog pd;
	ProgressThread pThread;
	
	public static Bitmap unnko;
	
	protected Handler handler;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		sp =  getSharedPreferences(PRE_FILE_NAME, MODE_PRIVATE);
		editor = sp.edit();
		
		subPath = Environment.getExternalStorageDirectory().getPath();
		appPath = this.getFilesDir().getPath();

//		lView = (ListView)findViewById(R.id.listview);
//		upBtn = (Button)findViewById(R.id.up_btn);
//		upBtn.setOnClickListener(this);
//		loginBtn = (Button)findViewById(R.id.login_btn);
//		loginBtn.setOnClickListener(this);
//		testBtn = (Button)findViewById(R.id.test_btn);
//		testBtn.setOnClickListener(this);
		
		handler = new Handler();
		
		unnko = BitmapFactory.decodeResource(getResources(), R.drawable.unnko);
	}
	
	
	//ダイアログを表示
	protected void dialogShow(String title, String mess, int id){
		dg = new AlertDialog.Builder(this);
		dg.setCancelable(false);
		dg.setTitle(title);								
		dg.setMessage(mess);
		if(id == 0){
			dg.setIcon(R.drawable.ic_launcher);
			dg.setPositiveButton("OK", this);
			dialog1 = dg.create();
			dialog1.show();
		}else if(id == 1){
			dg.setIcon(R.drawable.ic_launcher);
			dg.setPositiveButton("OK", this);
			dg.setNegativeButton("NO", this);
			dialog2 = dg.create();
			dialog2.show();
		}
	}
	
//	protected void customDialogShow1(){
//    	LayoutInflater inflater = this.getLayoutInflater();
//		View v = inflater.inflate(R.layout.member_regist_dialog, (ViewGroup)findViewById(R.id.lRoot));
//		dg = new AlertDialog.Builder(this);
//		Button imgSelectBtn = (Button)v.findViewById(R.id.img_select_btn);
//		nameETxt = (EditText)v.findViewById(R.id.name_edit);
//		passETxt = (EditText)v.findViewById(R.id.pass_edit);
//		imgPathTxt = (TextView)v.findViewById(R.id.img_path_text);
//		getPref();
//		imgSelectBtn.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				String[] itemArr = FileProcess.getFilesStr(subPath);
//				listDialogShow("tes", itemArr);
//			}
//		});
//    	dg.setView(v);
//		dg.setIcon(R.drawable.ic_launcher);
//		dg.setPositiveButton("OK", this);
//		dg.setNegativeButton("CANCEL", this);
//		dg.setTitle("アカウント作成");
//		customDialog1 = dg.create();
//		customDialog1.show();
//    }
	
	
//	protected void customDialogShow2(){
//    	LayoutInflater inflater = this.getLayoutInflater();
//		View v = inflater.inflate(R.layout.custom_dialog2, (ViewGroup)findViewById(R.id.lRoot));
//		dg = new AlertDialog.Builder(this);
//		comEtxt = (EditText)v.findViewById(R.id.comment_edit);
//    	dg.setView(v);
//		dg.setIcon(R.drawable.ic_launcher);
//		dg.setPositiveButton("OK", this);
//		dg.setNegativeButton("CANCEL", this);
//		dg.setTitle("つぶやく");
//		customDialog2 = dg.create();
//		customDialog2.show();
//    }
//	
	void getPref(){
    	nameETxt.setText( sp.getString("NAME", "") );
    	passETxt.setText (sp.getString("PASSWORD", "") );
	}
	
	
	void listDialogShow(String title, String[] itemArr){
		saveListItem = "";
  		dg = new AlertDialog.Builder(this);
  		dg.setTitle(title);							//タイトルなどの設定
  		dg.setIcon(R.drawable.ic_launcher);
  		dg.setSingleChoiceItems(itemArr, -1, this);
  		dg.setPositiveButton("OK", this);			//ボタンの設定　＊ボタンの最大値は３
  		dg.setNegativeButton("Canncel", this);
  		listDialog = dg.create();
  		listDialog.show();
  	}
	
	void progressShow(String str){
		pd = new ProgressDialog(this); 							//プログレスダイアログの作成
		pd.setTitle(str);
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);		//プログレス代ログのスタイルを円スタイルに設定します。
		pd.setIndeterminate(false);								//falseに設定しないとバーが進んでいかない。
		pd.setCancelable(false);
		pd.show();
	}
	
	//プログレスバーのスタート
	void startProgress(String str){
		progressShow(str);
		pThread = new ProgressThread(pd);
		pThread.doStart();
	}
	
	//プログレスバーのストップ
	void stopProgress(){
		if(pThread != null){
			pThread.doStop();
		}
	}
	
	//プログレスバー表示スレッド
	public class ProgressThread extends Thread {
		boolean swLoop;
		ProgressDialog pd;
		public ProgressThread(ProgressDialog pd){
			this.pd = pd;
		}
		@Override
		public void run() {
			super.run();
			while(swLoop){
					
			}
			pd.dismiss();
			pd = null;
		}
		public void doStart(){
			swLoop = true;
			start();
		}
			
		public void doStop(){
			swLoop = false;
		}
	}

	

	@Override
	public void onClick(DialogInterface dialog, int buttonId) {
		if(dialog == listDialog){
			if(buttonId == DialogInterface.BUTTON_POSITIVE){
				
				imgPathTxt.setText(saveListItem);
			}else if(buttonId == DialogInterface.BUTTON_NEGATIVE){
				
			}else{
				saveListItem = listDialog.getListView().getAdapter().getItem(buttonId).toString();
			}
		}
	}
	
	 public static void savePreference(String name, String password, String imgFileName, String imgUrl){
		 editor.putString("NAME", name);
		 editor.putString("PASSWORD",password);
		 editor.putString("IMG_FILE_NAME",imgFileName);
		 editor.putString("IMG_URL",imgUrl);
		 editor.commit();
	 }
	 
	 void setFormData(List<NameValuePair> formData, String mode){
		 String date = new CCal().getCurrentSecondStr();
		 formData.add(new BasicNameValuePair("mode", mode));
		 formData.add(new BasicNameValuePair("date", date));
		 formData.add(new BasicNameValuePair("name", checkBlank(sp.getString("NAME", sp.getString("NAME", "")) )));
		 formData.add(new BasicNameValuePair("password", checkBlank(sp.getString("PASSWORD", "")) ));
		 formData.add(new BasicNameValuePair("imgfilename", checkBlank(sp.getString("IMG_FILE_NAME", "") )));
		 formData.add(new BasicNameValuePair("imgurl", checkBlank(sp.getString("IMG_URL", "")) ));
	 }
	 
	 String checkBlank(String str){
		if(str.equals("")){
			str = "no";
		}
		return str;
	}


//	@Override
//	public void onClick(View v) {	
//	}

}
